package com.molchan.app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.molchan.app.model.json.ValuteUpdate;

import java.io.IOException;
import java.io.InputStream;

public class ParseJson {

    public static void main(String[] args) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        InputStream is = ParseJson.class.getResourceAsStream("/daily_json.js");

        ValuteUpdate valuteUpdate = mapper.readValue(is, ValuteUpdate.class);

        valuteUpdate.getValutes().forEach((k,v) ->{
            System.out.println("Valute Ticker: " + k);
            System.out.println("\tID: " + v.getId());
            System.out.println("\tCharCode: " + v.getCharCode());
            System.out.println("\tName: " + v.getName());
            System.out.println("\tNumCode: " + v.getNumCode());
            System.out.println("\tNominal: " + v.getNominal());
            System.out.println("\tValue: " + v.getValue());
            System.out.println("\tPrevious: " + v.getPrevious());
            System.out.println();
        });
    }
}
