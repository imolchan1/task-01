package com.molchan.app.model.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.molchan.app.model.Valute;

import java.util.Date;
import java.util.Map;

public class ValuteUpdate {

    @JsonProperty("Date")
    private Date date;
    @JsonProperty("PreviousDate")
    private Date previousDate;
    @JsonProperty("PreviousURL")
    private String previousURL;
    @JsonProperty("Timestamp")
    private Date timestamp;
    @JsonProperty("Valute")
    private Map<String, Valute> valutes;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getPreviousDate() {
        return previousDate;
    }

    public void setPreviousDate(Date previousDate) {
        this.previousDate = previousDate;
    }

    public String getPreviousURL() {
        return previousURL;
    }

    public void setPreviousURL(String previousURL) {
        this.previousURL = previousURL;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Map<String, Valute> getValutes() {
        return valutes;
    }

    public void setValutes(Map<String, Valute> valutes) {
        this.valutes = valutes;
    }
}
