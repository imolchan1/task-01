package com.molchan.app;


import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.molchan.app.model.xml.ValCurs;

import java.io.IOException;
import java.io.InputStream;

public class ParseXml
{
    public static void main( String[] args ) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        InputStream is = ParseJson.class.getResourceAsStream("/daily_eng_utf8.xml");

        ValCurs valCurs = xmlMapper.readValue(is, ValCurs.class);

        valCurs.getValute().forEach( v -> {
            System.out.println("CharCode: " + v.getCharCode());
            System.out.println("\tID: " + v.getId());
            System.out.println("\tName: " + v.getName());
            System.out.println("\tNumCode: " + v.getNumCode());
            System.out.println("\tNominal: " + v.getNominal());
            System.out.println("\tValue: " + v.getValue());
            System.out.println();
        });
    }
}
